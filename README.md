# s6-ports

If you are interested in collaborating on port maintenance, please read the **CommitStyle** included in [CONTRIBUTING](https://codeberg.org/venomlinux/main/src/branch/main/CONTRIBUTING.md) 

## Implementation of S6 init in Venom Linux
It is recommended to make the change of init from a chroot. A Venom Linux ISO Live can be used to perform the chroot cage and from there, follow the following instructions:


### Add s6 repo in '/etc/scratchpkg.repo' before main
```
/usr/ports/s6   https://gitlab.com/venomlinux/ports/s6
/usr/ports/main https://gitlab.com/venomlinux/ports/main
```


### Sync repos
`# portsync -r`

### Install s6 base
`# scratch install base-s6`

### Check pkgs to install
1. skalibs
2. execline
3. s6
4. s6-rc
5. s6-base
6. s6-scripts
7. s6-linux-init
8. syslogd-s6
9. base-s6

### Check that s6log user has been created
`# grep s6log /etc/passwd`

### Install needed services
`# scratch install seatd-s6 dhcpcd-s6 wpa_supplicant-s6 ...`

### Add needed services to default to run at boot
`# s6-service add default foo1 foo2 foo3...`

### Configure wpa_supplicant service or any other in '/etc/s6/config/foo'
`# vim /etc/s6/config/wpa_supplicant.conf`

### Compile s6 db services
`# s6-db-reload -r`

### Exit chroot
`# exit`

### Reboot system
`# reboot`

### In s6-system up, check enabled services
`# s6-rc-db atomics default`

### Basic usage
- Stop a service/bundle 
    - `# s6-rc -d change service_name`
    - `# s6-rc stop service_name`

- Start a service/bundle 
    - `# s6-rc -u change service_name`
    - `# s6-rc start service_name`

- Restart a service (only works with s6-rc longruns)
    - `# s6-svc -r /run/service/service_name`

- List all active services 
    - `# s6-rc -a list`

- List all services/bundles in the database 
    - `# s6-rc-db list all`

- Check the status of an s6-rc longrun 
    - `# s6-svstat /run/service/service_name`

- Manage bundles contents with s6-service script:
    - Add: _s6-service add bundlename contents_
        - `# s6-service add default dhcpcd`
    - Delete: _s6-service delete bundlename contents_
        - `# s6-service delete default dhcpcd`

### Use rc-local service to set static network config or to run anything at boot
- Add your device and ip settings in `/etc/s6/rc.local`
    ```
    # Example static ip configuration:
    ip link set enp3s0 up
    ip a add 192.168.1.2/24 dev enp3s0
    iproute add default via 192.168.1.1 dev enp3s0
    ```
- Recompile db
    - `s6-db-reload -r`

### Clean unwanted services, we use ttys as example
S6 have `tty{1,6}` enabled by default plus one in reserve `ttyS`.
We're gonna delete tty{3,6}
- Remove the services 
    - `# rm -r /etc/s6/sv/ttyX`
- Remove getty dependencies
    - `# rm /etc/s6/sv/getty/contents.d/ttyX`

- Recompile ddbb
    - `s6-db-reload -r`


### More info:
- https://skarnet.org/software/s6-rc
- https://skarnet.org/software/s6-rc/faq.html
- https://wiki.artixlinux.org/Main/S6
