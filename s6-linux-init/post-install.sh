#!/bin/sh

# Add log group and user
getent group s6log >/dev/null || groupadd -r s6log

getent passwd s6log >/dev/null || \
useradd -r -M -g s6log -G s6log -c "s6-log user" -d /dev/null -s /sbin/nologin s6log

# Compile service db
s6-db-reload -r
