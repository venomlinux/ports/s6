#!/bin/sh

SYSV="sysvinit init rc sysklogd"
RUNIT="runit runit-rc sysklogd"
DINIT="dinit dinit-rc"

for i in "$DINIT" "$RUNIT" "$SYSV" ; do
	scratch isinstalled $i && scratch remove -y $i
done
